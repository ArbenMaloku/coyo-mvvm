package com.arbenmaloku.coyochallange.application

import android.app.Application
import com.arbenmaloku.coyochallange.di.component.AppComponent
import com.arbenmaloku.coyochallange.di.component.DaggerAppComponent
import com.arbenmaloku.coyochallange.di.module.AppModule
import com.arbenmaloku.coyochallange.di.module.NetModule
import com.arbenmaloku.coyochallange.utils.Constants

class App : Application() {

    val component: AppComponent by lazy {
        DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .netModule(NetModule(Constants.BASE_URL))
            .build()
    }

    override fun onCreate() {
        super.onCreate()

    }


}