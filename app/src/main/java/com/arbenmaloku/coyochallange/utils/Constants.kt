package com.arbenmaloku.coyochallange.utils

class Constants {
    companion object {
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    }

    enum class BundleParams {
        UserId,
        PostId,
        PostTitle,
        PostObject
    }


}