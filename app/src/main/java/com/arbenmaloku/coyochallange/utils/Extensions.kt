package com.arbenmaloku.coyochallange.utils

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment
import android.widget.Toast
import com.arbenmaloku.coyochallange.application.App

/**
 * Show specific string as toast ex. "Hello world".toast(context)ss
 */
fun Any.toast(context: Context?, duration: Int = Toast.LENGTH_SHORT): Toast {
    return Toast.makeText(context, this.toString(), duration).apply { show() }
}


val Activity.app: App get() = application as App

val Fragment.app: App? get() = activity?.application as App