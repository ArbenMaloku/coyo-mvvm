package com.arbenmaloku.coyochallange.data.source.repository

import com.arbenmaloku.coyochallange.data.source.local.UserDao
import com.arbenmaloku.coyochallange.data.source.models.User
import com.arbenmaloku.coyochallange.data.source.remote.ApiInterface
import com.arbenmaloku.coyochallange.utils.Utils
import io.reactivex.Observable
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val apiInterface: ApiInterface, private val userDao: UserDao, private val utils: Utils
) {


    /**
     * Use this function to get a list of users.
     * If there is network connection it will concat the request, otherwise would load from local storage if there are any.
     *
     * @return List of users
     */
    fun getUsers(): Observable<List<User>> {

        val hasConnection = utils.isConnectedToInternet()
        var observableFromAPI: Observable<List<User>>? = null

        if (hasConnection) {
            observableFromAPI = getUserFromAPI()
        }

        val observableFromDb = getUserFromDB()

        return if (hasConnection) {
            Observable.concatArrayEager(observableFromDb, observableFromAPI)
        } else {
            observableFromDb
        }
    }

    private fun getUserFromAPI(): Observable<List<User>> {
        return apiInterface.getUsers()
            .doOnNext { user ->
                userDao.insertUsers(user)
            }
    }

    private fun getUserFromDB(): Observable<List<User>> {
        return userDao.getUsers()
            .toObservable()
            .doOnNext { }
    }

    private fun getUserByIdFromDb(userId: Int): Observable<User> {
        return userDao.getUserById(userId).toObservable()

    }


    fun getUser(userId: Int): Observable<User> {
        return getUserByIdFromDb(userId)

    }
}