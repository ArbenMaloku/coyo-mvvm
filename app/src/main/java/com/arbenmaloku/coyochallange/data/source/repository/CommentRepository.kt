package com.arbenmaloku.coyochallange.data.source.repository

import com.arbenmaloku.coyochallange.data.source.local.CommentDao
import com.arbenmaloku.coyochallange.data.source.models.Comment
import com.arbenmaloku.coyochallange.data.source.remote.ApiInterface
import com.arbenmaloku.coyochallange.utils.Utils
import io.reactivex.Observable
import javax.inject.Inject

class CommentRepository @Inject constructor(
    private val apiInterface: ApiInterface, private val commentDao: CommentDao, private val utils: Utils
) {


    /**
     * Use this function to get a list of Comments for a specific post.
     * If there is network connection it will concat the request, otherwise would load from local storage if there are any.
     *
     * @return List of Comments
     */
    fun getComments(postId: Int): Observable<List<Comment>> {

        val hasConnection = utils.isConnectedToInternet()
        var observableFromAPI: Observable<List<Comment>>? = null

        if (hasConnection) {
            observableFromAPI = getCommentsFromApi(postId)
        }

        val observableFromDb = getCommentsFromDb(postId)

        return if (hasConnection) {
            Observable.concatArrayEager(observableFromAPI, observableFromDb)
        } else {
            observableFromDb
        }
    }

    private fun getCommentsFromApi(postId: Int): Observable<List<Comment>> {
        return apiInterface.getComments(postId)
            .doOnNext { comments ->
                commentDao.insertComments(comments)
            }
    }

    private fun getCommentsFromDb(postId: Int): Observable<List<Comment>> {
        return commentDao.getComments(postId)
            .toObservable()
            .doOnNext { }
    }
}