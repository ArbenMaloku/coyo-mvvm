package com.arbenmaloku.coyochallange.data.source.repository

import com.arbenmaloku.coyochallange.data.source.local.PostDao
import com.arbenmaloku.coyochallange.data.source.models.Post
import com.arbenmaloku.coyochallange.data.source.remote.ApiInterface
import com.arbenmaloku.coyochallange.utils.Utils
import io.reactivex.Observable
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val apiInterface: ApiInterface,
    private val postDao: PostDao,
    private val utils: Utils
) {

    /**
     * Use this function to get a list of Posts.
     * If there is network connection it will concat the request, otherwise would load from local storage if there are any.
     *
     * @param userId - UserId
     * @return List of post
     */
    fun getPosts(userId: Int): Observable<List<Post>> {

        val hasConnection = utils.isConnectedToInternet()
        var observableFromAPI: Observable<List<Post>>? = null

        if (hasConnection) {
            observableFromAPI = getPostFromAPI(userId)
        }

        val observableFromDb = getUserFromDB(userId)

        return if (hasConnection) {
            Observable.concatArrayEager(observableFromDb, observableFromAPI)
        } else {
            observableFromDb
        }
    }

    private fun getPostFromAPI(userId: Int): Observable<List<Post>> {
        return apiInterface.getPosts(userId)
            .doOnNext { posts ->
                postDao.insertPosts(posts)
            }
    }

    private fun getUserFromDB(userId: Int): Observable<List<Post>> {
        return postDao.getPosts(userId)
            .toObservable()
            .doOnNext { }
    }
}