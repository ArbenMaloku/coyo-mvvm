package com.arbenmaloku.coyochallange.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.arbenmaloku.coyochallange.data.source.models.Comment
import io.reactivex.Single

@Dao
interface CommentDao {
    @Query("SELECT * FROM comments where postId = :postId")
    fun getComments(postId: Int): Single<List<Comment>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertComments(comments: List<Comment>)


}