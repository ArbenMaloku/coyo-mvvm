package com.arbenmaloku.coyochallange.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.arbenmaloku.coyochallange.data.source.models.Post
import io.reactivex.Single


@Dao
interface PostDao {
    @Query("SELECT * FROM posts where userId = :userId")
    fun getPosts(userId: Int): Single<List<Post>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPosts(user: List<Post>)


}