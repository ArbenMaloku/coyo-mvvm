package com.arbenmaloku.coyochallange.data.source.models

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.*
import java.util.*

@Entity(tableName = "Comments")

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("postId", "id", "name", "email", "body")
class Comment {


    @JsonProperty("postId")
    @get:JsonProperty("postId")
    @set:JsonProperty("postId")
    var postId: Int = 0

    @PrimaryKey
    @JsonProperty("id")
    @get:JsonProperty("id")
    @set:JsonProperty("id")
    var id: Int? = null

    @JsonProperty("name")
    @get:JsonProperty("name")
    @set:JsonProperty("name")
    var name: String? = null

    @JsonProperty("email")
    @get:JsonProperty("email")
    @set:JsonProperty("email")
    var email: String? = null

    @JsonProperty("body")
    @get:JsonProperty("body")
    @set:JsonProperty("body")
    var body: String? = null

    @Ignore
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @Ignore
    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @Ignore
    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}