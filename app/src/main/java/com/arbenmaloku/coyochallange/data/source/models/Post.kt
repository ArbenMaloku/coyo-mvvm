package com.arbenmaloku.coyochallange.data.source.models

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.*
import java.util.*

@Entity(tableName = "Posts")

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("userId", "id", "title", "body")
class Post():Parcelable {

    @PrimaryKey
    @JsonProperty("id")
    @get:JsonProperty("id")
    @set:JsonProperty("id")
    var id: Int = 0

    @JsonProperty("userId")
    @get:JsonProperty("userId")
    @set:JsonProperty("userId")
    var userId: Int = 0

    @JsonProperty("title")
    @get:JsonProperty("title")
    @set:JsonProperty("title")
    var title: String? = null

    @JsonProperty("body")
    @get:JsonProperty("body")
    @set:JsonProperty("body")
    var body: String? = null

    @Ignore
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()


    @Ignore
    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @Ignore
    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }


    @Ignore
    @JsonIgnore
    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        userId = parcel.readInt()
        title = parcel.readString()
        body = parcel.readString()
    }


    @Ignore
    @JsonIgnore
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(userId)
        parcel.writeString(title)
        parcel.writeString(body)
    }

    @Ignore
    @JsonIgnore
    override fun describeContents(): Int {
        return 0
    }


    companion object CREATOR : Parcelable.Creator<Post> {
        override fun createFromParcel(parcel: Parcel): Post {
            return Post(parcel)
        }

        override fun newArray(size: Int): Array<Post?> {
            return arrayOfNulls(size)
        }
    }

}