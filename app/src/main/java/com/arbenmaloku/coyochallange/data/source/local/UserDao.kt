package com.arbenmaloku.coyochallange.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.arbenmaloku.coyochallange.data.source.models.User
import io.reactivex.Single


@Dao
interface UserDao {
    @Query("SELECT * FROM users")
    fun getUsers(): Single<List<User>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(user: List<User>)

    @Query("SELECT * FROM users where id = :userId")
    fun getUserById(userId: Int): Single<User>


}