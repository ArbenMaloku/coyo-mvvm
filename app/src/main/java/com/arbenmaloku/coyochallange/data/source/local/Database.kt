package com.arbenmaloku.coyochallange.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.arbenmaloku.coyochallange.data.source.models.Comment
import com.arbenmaloku.coyochallange.data.source.models.Post
import com.arbenmaloku.coyochallange.data.source.models.User


@Database(entities = [User::class, Post::class, Comment::class], version = 2, exportSchema = false)
abstract class Database : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun postDao(): PostDao
    abstract fun commentDao(): CommentDao
}
