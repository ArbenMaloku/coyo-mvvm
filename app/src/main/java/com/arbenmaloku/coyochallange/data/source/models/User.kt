package com.arbenmaloku.coyochallange.data.source.models

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.*
import java.util.*

@Entity(
    tableName = "Users"
)

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("id", "name", "username", "email", "address", "phone", "website", "company")
class User {

    @PrimaryKey
    @JsonProperty("id")
    @get:JsonProperty("id")
    @set:JsonProperty("id")
    var id: Int = 0

    @JsonProperty("name")
    @get:JsonProperty("name")
    @set:JsonProperty("name")
    var name: String? = null

    @JsonProperty("username")
    @get:JsonProperty("username")
    @set:JsonProperty("username")
    var username: String? = null

    @JsonProperty("email")
    @get:JsonProperty("email")
    @set:JsonProperty("email")
    var email: String? = null

   /* @JsonProperty("address")
    @get:JsonProperty("address")
    @set:JsonProperty("address")
    var address: Address? = null*/

    @JsonProperty("phone")
    @get:JsonProperty("phone")
    @set:JsonProperty("phone")
    var phone: String? = null

    @JsonProperty("website")
    @get:JsonProperty("website")
    @set:JsonProperty("website")
    var website: String? = null

   /* @JsonProperty("company")
    @get:JsonProperty("company")
    @set:JsonProperty("company")
    var company: Company? = null*/

    @Ignore
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @Ignore
    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @Ignore
    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}