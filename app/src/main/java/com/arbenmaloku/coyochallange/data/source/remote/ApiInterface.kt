package com.arbenmaloku.coyochallange.data.source.remote

import com.arbenmaloku.coyochallange.data.source.models.Comment
import com.arbenmaloku.coyochallange.data.source.models.Post
import com.arbenmaloku.coyochallange.data.source.models.User
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("users")
    fun getUsers(): Observable<List<User>>

    @GET("posts")
    fun getPosts(@Query("userId") userId: Int): Observable<List<Post>>

    @GET("comments")
    fun getComments(@Query("postId") postId: Int): Observable<List<Comment>>
}