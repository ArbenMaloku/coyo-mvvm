package com.arbenmaloku.coyochallange.ui.fragments

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arbenmaloku.coyochallange.R
import com.arbenmaloku.coyochallange.data.source.models.Comment
import com.arbenmaloku.coyochallange.data.source.models.Post
import com.arbenmaloku.coyochallange.data.source.models.User
import com.arbenmaloku.coyochallange.ui.adapters.CommentsAdapter
import com.arbenmaloku.coyochallange.ui.viewmodels.CommentViewModel
import com.arbenmaloku.coyochallange.ui.viewmodels.CommentViewModelFactory
import com.arbenmaloku.coyochallange.utils.Constants
import com.arbenmaloku.coyochallange.utils.app
import com.arbenmaloku.coyochallange.utils.toast
import kotlinx.android.synthetic.main.fragment_comments.*
import javax.inject.Inject

class CommentsFragment : Fragment() {
    //region Declarations
    @Inject
    lateinit var commentViewModelFactory: CommentViewModelFactory

    private lateinit var commentViewModel: CommentViewModel
    private lateinit var commentsAdapter: CommentsAdapter

    //endregion

    //region Override methods
    override fun onCreate(savedInstanceState: Bundle?) {
        app?.component?.inject(this)
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comments, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }


    override fun onDetach() {
        commentViewModel.disposeElements()
        super.onDetach()
    }
    //endregion

    //region Custom methods

    private fun initialize() {
        commentViewModel = ViewModelProviders.of(this, commentViewModelFactory).get(CommentViewModel::class.java)

        commentsAdapter = CommentsAdapter()
        recyclerViewComments.layoutManager = LinearLayoutManager(context)
        recyclerViewComments.adapter = commentsAdapter

        recyclerViewComments.emptyStateView = tvNoData
        recyclerViewComments.loadingStateView = progressBar

        val post = activity?.intent?.getParcelableExtra<Post>(Constants.BundleParams.PostObject.name)

        post?.let {

            tvContent.text = post.body

            commentViewModel.loadComments(post.id)
            commentViewModel.result().observe(this, Observer<List<Comment>> { comments ->
                comments?.let {
                    tvCommentCount.text = String.format(getString(R.string.comment_count_format), comments.size)
                    commentsAdapter.addComments(comments)
                }
            })
            commentViewModel.error().observe(this, Observer<String> { error ->
                error?.toast(context)
            })

            commentViewModel.loadUser(post.userId)
            commentViewModel.userResult().observe(this, Observer<User> { user ->
                user?.let {
                    tvAuthor.text = user.name
                }
            })

        }


    }
    //endregion

}
