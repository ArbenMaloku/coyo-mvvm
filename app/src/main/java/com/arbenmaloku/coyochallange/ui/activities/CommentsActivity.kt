package com.arbenmaloku.coyochallange.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import com.arbenmaloku.coyochallange.R
import kotlinx.android.synthetic.main.activity_main.*

class CommentsActivity : AppCompatActivity() {
    //region Override methods
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)

        initialize()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }
    //endregion

    //region Custom methods
    private fun initialize() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        toolbar.title = getString(R.string.post_details)
        setSupportActionBar(toolbar)
    }

    //endregion
}
