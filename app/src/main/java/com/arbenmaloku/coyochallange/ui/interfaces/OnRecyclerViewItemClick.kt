package com.arbenmaloku.coyochallange.ui.interfaces

import android.view.View

interface OnRecyclerViewItemClick {
    fun onItemClick(view: View?, position: Int)
    fun onItemLongClick(view: View?, position: Int)
}