package com.arbenmaloku.coyochallange.ui.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.arbenmaloku.coyochallange.R
import com.arbenmaloku.coyochallange.data.source.models.Comment
import com.arbenmaloku.coyochallange.ui.interfaces.OnRecyclerViewItemClick
import kotlinx.android.synthetic.main.custom_comment_list_item.view.*

class CommentsAdapter : RecyclerView.Adapter<CommentsAdapter.CommentViewHolder>() {

    //region Declarations
    private var commentList = mutableListOf<Comment>()
    var mOnRecyclerViewItemClick: OnRecyclerViewItemClick? = null
    //endregion

    //region Override methods
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): CommentViewHolder {
        return CommentViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.custom_comment_list_item,
                        parent,
                        false
                )
        )
    }

    override fun getItemCount(): Int {
        return commentList.size
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bind(commentList[position])
    }

    //endregion

    //region Custom methods
    fun addComments(comment: List<Comment>) {
        commentList = comment.toMutableList()
        notifyDataSetChanged()
    }

    //endregion

    //region View Holders
    inner class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        val container: View = itemView.container
        private val tvComment: TextView = itemView.tvComment

        init {
            container.setOnClickListener(this)
        }

        fun bind(post: Comment) {
            tvComment.text = post.body
        }

        override fun onClick(view: View) {
            mOnRecyclerViewItemClick?.onItemClick(view, adapterPosition)
        }
    }
    //endregion
}
