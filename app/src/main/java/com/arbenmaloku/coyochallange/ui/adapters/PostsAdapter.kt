package com.arbenmaloku.coyochallange.ui.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.arbenmaloku.coyochallange.R
import com.arbenmaloku.coyochallange.data.source.models.Post
import kotlinx.android.synthetic.main.custom_user_list_item.view.*

class PostsAdapter(val onItemClick: (position: Int) -> Unit) : RecyclerView.Adapter<PostsAdapter.PostViewHolder>() {

    //region Declarations
    private var postsList = mutableListOf<Post>()
    //endregion

    //region Override methods
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): PostViewHolder {
        return PostViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.custom_user_list_item,
                        parent,
                        false
                )
        )
    }

    override fun getItemCount(): Int {
        return postsList.size
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(postsList[position])
    }

    //endregion

    //region Custom methods
    fun addPosts(posts: List<Post>) {
        postsList = posts.toMutableList()
        notifyDataSetChanged()
    }

    fun getPost(position: Int): Post {
        return postsList[position]
    }
    //endregion

    //region View Holders
    inner class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        val container: View = itemView.container
        private val tvName: TextView = itemView.tvName
        private val tvPhone: TextView = itemView.tvPhone

        init {
            container.setOnClickListener(this)
        }

        fun bind(post: Post) {
            tvName.text = post.title
            tvPhone.text = post.body
        }

        override fun onClick(view: View) {
            onItemClick(adapterPosition)
        }
    }
    //endregion
}
