package com.arbenmaloku.coyochallange.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arbenmaloku.coyochallange.data.source.models.Comment
import com.arbenmaloku.coyochallange.data.source.models.User
import com.arbenmaloku.coyochallange.data.source.repository.CommentRepository
import com.arbenmaloku.coyochallange.data.source.repository.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CommentViewModel @Inject constructor(private val commentRepository: CommentRepository, private val userRepository: UserRepository) : ViewModel() {
    var result: MutableLiveData<List<Comment>> = MutableLiveData()
    var userResult: MutableLiveData<User> = MutableLiveData()
    var error: MutableLiveData<String> = MutableLiveData()

    private lateinit var disposableObserver: DisposableObserver<List<Comment>>
    private lateinit var disposableUserObserver: DisposableObserver<User>

    fun result(): LiveData<List<Comment>> {
        return result
    }

    fun userResult(): LiveData<User> {
        return userResult
    }

    fun error(): LiveData<String> {
        return error
    }


    fun loadComments(postId: Int) {

        disposableObserver = object : DisposableObserver<List<Comment>>() {
            override fun onComplete() {

            }

            override fun onNext(posts: List<Comment>) {
                result.postValue(posts)
            }

            override fun onError(e: Throwable) {
                error.postValue(e.message)
            }
        }

        commentRepository.getComments(postId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribe(disposableObserver)
    }

    fun loadUser(userId: Int) {
        disposableUserObserver = object : DisposableObserver<User>() {
            override fun onComplete() {

            }

            override fun onNext(user: User) {
                userResult.postValue(user)
            }

            override fun onError(e: Throwable) {
                error.postValue(e.message)
            }
        }

        userRepository.getUser(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribe(disposableUserObserver)
    }

    fun disposeElements() {
        if (!disposableObserver.isDisposed) disposableObserver.dispose()
    }
}