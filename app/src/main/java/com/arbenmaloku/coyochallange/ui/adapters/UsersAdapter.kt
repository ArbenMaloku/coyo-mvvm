package com.arbenmaloku.coyochallange.ui.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.arbenmaloku.coyochallange.R
import com.arbenmaloku.coyochallange.data.source.models.User
import kotlinx.android.synthetic.main.custom_user_list_item.view.*

class UsersAdapter(val onItemClick: (position: Int) -> Unit) : RecyclerView.Adapter<UsersAdapter.UserViewHolder>() {

    //region Declarations
    private var usersList = mutableListOf<User>()
    //endregion

    //region Override methods
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): UserViewHolder {
        return UserViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.custom_user_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return usersList.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(usersList[position])
    }

    //endregion

    //region Custom methods
    fun addUsers(users: List<User>) {
        usersList = users.toMutableList()
        notifyDataSetChanged()
    }

    fun getUser(position: Int): User {
        return usersList[position]
    }
    //endregion

    //region View Holders
    inner class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        val container: View = itemView.container
        private val tvName: TextView = itemView.tvName
        private val tvPhone: TextView = itemView.tvPhone

        init {
            container.setOnClickListener(this)
        }

        fun bind(user: User) {
            tvName.text = user.name
            tvPhone.text = user.phone
        }

        override fun onClick(view: View) {
            onItemClick(adapterPosition)
        }
    }
    //endregion
}
