package com.arbenmaloku.coyochallange.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class CommentViewModelFactory @Inject constructor(private val commentViewModel: CommentViewModel) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CommentViewModel::class.java)) {
            return commentViewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}