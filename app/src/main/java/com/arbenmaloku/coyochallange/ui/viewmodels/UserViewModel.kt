package com.arbenmaloku.coyochallange.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arbenmaloku.coyochallange.data.source.models.User
import com.arbenmaloku.coyochallange.data.source.repository.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class UserViewModel @Inject constructor(private val userRepository: UserRepository) : ViewModel() {
    var result: MutableLiveData<List<User>> = MutableLiveData()
    var error: MutableLiveData<String> = MutableLiveData()
    private lateinit var disposableObserver: DisposableObserver<List<User>>

    fun result(): LiveData<List<User>> {
        return result
    }

    fun error(): LiveData<String> {
        return error
    }

    fun loadUsers() {

        disposableObserver = object : DisposableObserver<List<User>>() {
            override fun onComplete() {

            }

            override fun onNext(users: List<User>) {
                result.postValue(users)
            }

            override fun onError(e: Throwable) {
                error.postValue(e.message)
            }
        }

        userRepository.getUsers()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe(disposableObserver)
    }

    fun disposeElements() {
        if (!disposableObserver.isDisposed) disposableObserver.dispose()
    }
}