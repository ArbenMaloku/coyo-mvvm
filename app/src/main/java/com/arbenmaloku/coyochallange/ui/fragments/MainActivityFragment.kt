package com.arbenmaloku.coyochallange.ui.fragments

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arbenmaloku.coyochallange.R
import com.arbenmaloku.coyochallange.data.source.models.User
import com.arbenmaloku.coyochallange.ui.activities.PostsActivity
import com.arbenmaloku.coyochallange.ui.adapters.UsersAdapter
import com.arbenmaloku.coyochallange.ui.customviews.RecyclerViewEmptyLoadingSupport
import com.arbenmaloku.coyochallange.ui.viewmodels.UserViewModel
import com.arbenmaloku.coyochallange.ui.viewmodels.UserViewModelFactory
import com.arbenmaloku.coyochallange.utils.Constants
import com.arbenmaloku.coyochallange.utils.app
import com.arbenmaloku.coyochallange.utils.toast
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

/**
 * A placeholder fragment containing a simple view.
 */
class MainActivityFragment : Fragment() {

    //region declarations
    @Inject
    lateinit var userViewModelFactory: UserViewModelFactory

    lateinit var userViewModel: UserViewModel
    lateinit var usersAdapter: UsersAdapter

    //endregion

    //region Override methods
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        app?.component?.inject(this)
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    override fun onDetach() {
        userViewModel.disposeElements()
        super.onDetach()
    }
    //endregion


    //region Custom methods

    private fun initialize() {
        userViewModel = ViewModelProviders.of(this, userViewModelFactory).get(UserViewModel::class.java)

        usersAdapter = UsersAdapter { position ->
            val user = usersAdapter.getUser(position)

            startActivity(
                Intent(
                    context,
                    PostsActivity::class.java
                ).apply { putExtra(Constants.BundleParams.UserId.name, user.id) })
        }


        recyclerViewUsers.layoutManager = LinearLayoutManager(context)
        recyclerViewUsers.loadingStateView = progressBar
        recyclerViewUsers.emptyStateView = tvNoData
        recyclerViewUsers.stateView = RecyclerViewEmptyLoadingSupport.State.LOADING


        recyclerViewUsers.adapter = usersAdapter


        userViewModel.loadUsers()

        userViewModel.result().observe(this, Observer<List<User>> { users ->
            users?.let { usersAdapter.addUsers(it) }
        })

        userViewModel.error().observe(this, Observer<String?> { error ->
            error?.toast(context)
        })
    }

    //endregion
}
