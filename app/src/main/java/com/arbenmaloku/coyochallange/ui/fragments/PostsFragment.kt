package com.arbenmaloku.coyochallange.ui.fragments

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arbenmaloku.coyochallange.R
import com.arbenmaloku.coyochallange.data.source.models.Post
import com.arbenmaloku.coyochallange.ui.activities.CommentsActivity
import com.arbenmaloku.coyochallange.ui.adapters.PostsAdapter
import com.arbenmaloku.coyochallange.ui.viewmodels.PostViewModel
import com.arbenmaloku.coyochallange.ui.viewmodels.PostViewModelFactory
import com.arbenmaloku.coyochallange.utils.Constants
import com.arbenmaloku.coyochallange.utils.app
import com.arbenmaloku.coyochallange.utils.toast
import kotlinx.android.synthetic.main.fragment_posts.*
import javax.inject.Inject

class PostsFragment : Fragment() {
    //region Declarations
    @Inject
    lateinit var postViewModelFactory: PostViewModelFactory

    lateinit var postViewModel: PostViewModel
    lateinit var postsAdapter: PostsAdapter

    //endregion

    //region Override methods
    override fun onCreate(savedInstanceState: Bundle?) {
        app?.component?.inject(this)
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_posts, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }


    override fun onDetach() {
        postViewModel.disposeElements()
        super.onDetach()
    }
    //endregion

    //region Custom methods

    private fun initialize() {
        postViewModel = ViewModelProviders.of(this, postViewModelFactory).get(PostViewModel::class.java)

        postsAdapter = PostsAdapter { position ->
            startActivity(Intent(context, CommentsActivity::class.java).apply {
                putExtra(Constants.BundleParams.PostObject.name, postsAdapter.getPost(position))
            })
        }

        recyclerViewPosts.layoutManager = LinearLayoutManager(context)
        recyclerViewPosts.adapter = postsAdapter

        recyclerViewPosts.emptyStateView = tvNoData
        recyclerViewPosts.loadingStateView = progressBar

        val userId = activity?.intent?.getIntExtra(Constants.BundleParams.UserId.name, 0) ?: 0

        postViewModel.loadPosts(userId)
        postViewModel.result().observe(this, Observer<List<Post>> { posts ->
            posts?.let { postsAdapter.addPosts(it) }
        })
        postViewModel.error().observe(this, Observer<String> { error ->
            error?.toast(context)
        })
    }
    //endregion

    //region API calls


    //endregion

    //region Companion objects
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment [PostsFragment].
         */
        @JvmStatic
        fun newInstance() =
            PostsFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
    //endregion

}
