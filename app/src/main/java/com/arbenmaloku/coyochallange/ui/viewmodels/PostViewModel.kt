package com.arbenmaloku.coyochallange.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arbenmaloku.coyochallange.data.source.models.Post
import com.arbenmaloku.coyochallange.data.source.repository.PostRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PostViewModel @Inject constructor(private val postRepository: PostRepository) : ViewModel() {
    var result: MutableLiveData<List<Post>> = MutableLiveData()
    var error: MutableLiveData<String> = MutableLiveData()

    private lateinit var disposableObserver: DisposableObserver<List<Post>>

    fun result(): LiveData<List<Post>> {
        return result
    }

    fun error(): LiveData<String> {
        return error
    }

    fun loadPosts(userId: Int) {

        disposableObserver = object : DisposableObserver<List<Post>>() {
            override fun onComplete() {

            }

            override fun onNext(posts: List<Post>) {
                result.postValue(posts)
            }

            override fun onError(e: Throwable) {
                error.postValue(e.message)
            }
        }

        postRepository.getPosts(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribe(disposableObserver)
    }

    fun disposeElements() {
        if (!disposableObserver.isDisposed) disposableObserver.dispose()
    }
}