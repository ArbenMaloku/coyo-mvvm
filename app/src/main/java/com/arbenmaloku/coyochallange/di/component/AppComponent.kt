package com.arbenmaloku.coyochallange.di.component

import android.app.Application
import com.arbenmaloku.coyochallange.di.module.AppModule
import com.arbenmaloku.coyochallange.di.module.NetModule
import com.arbenmaloku.coyochallange.ui.fragments.CommentsFragment
import com.arbenmaloku.coyochallange.ui.fragments.MainActivityFragment
import com.arbenmaloku.coyochallange.ui.fragments.PostsFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetModule::class])
interface AppComponent {
    fun inject(app: Application)
    fun inject(mainActivityFragment: MainActivityFragment)
    fun inject(postsFragment: PostsFragment)
    fun inject(commentsFragment: CommentsFragment)
}