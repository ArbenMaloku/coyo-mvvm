package com.arbenmaloku.coyochallange.di.module

import androidx.room.Room
import android.content.Context
import com.arbenmaloku.coyochallange.data.source.local.CommentDao
import com.arbenmaloku.coyochallange.data.source.local.Database
import com.arbenmaloku.coyochallange.data.source.local.PostDao
import com.arbenmaloku.coyochallange.data.source.local.UserDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val app: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return app
    }

    @Provides
    @Singleton
    fun provideDatabase(context: Context): Database = Room.databaseBuilder(
            context, Database::class.java, "coyo_db"
    )
            .build()


    @Provides
    @Singleton
    fun provideUserDao(database: Database): UserDao = database.userDao()

    @Provides
    @Singleton
    fun providePostDao(database: Database): PostDao = database.postDao()

    @Provides
    @Singleton
    fun provideCommentsDao(database: Database): CommentDao = database.commentDao()

}