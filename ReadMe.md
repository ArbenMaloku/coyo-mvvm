# MVVM #

## Developers ##

	Arben Maloku - 23.11.2018

## Current libs ##
    'com.android.support:appcompat-v7'                  - 28.0.0
    'com.android.support.constraint:constraint-layout'  - 1.1.3'
    'com.google.dagger:dagger-android'                  - 2.19
    'com.google.dagger:dagger-compiler'                 - 2.19
    'com.google.dagger:dagger-android-processor'        - 2.19
    'com.fasterxml.jackson.core:jackson-core'           - 2.7.3
    'com.fasterxml.jackson.core:jackson-annotations'    - 2.7.3
    'com.fasterxml.jackson.core:jackson-databind'       - 2.7.3
    'android.arch.persistence.room:runtime'             - 2.7.3
    'android.arch.persistence.room:compiler'            - 2.7.3
    'android.arch.persistence.room:rxjava2'             - 1.1.1
    'android.arch.lifecycle:extension'                  - 1.1.1
    'android.arch.lifecycle:compiler'                   - 1.1.1
    'com.squareup.retrofit2:retrofit'                   - 2.5.0
    'com.squareup.retrofit2:converter-jackson'          - 2.5.0
    'com.squareup.retrofit2:adapter-rxjava2'            - 2.5.0
    'com.squareup.okhttp3:logging-interceptor'          - 3.9.1
    'io.reactivex.rxjava2:rxjava'                       - 2.2.3
    'io.reactivex.rxjava2:rxandroid'                    - 2.1.0
    
## Architecture

    MVVM architecture is used for this project. This architecture is used because it gives more opportunity to change,
        maintain and add test later.
        
    Daqger2 is used for Dependency Injections, Room as persistence (save data locally) and Retrofit to communicate with API.    

## Functionality

    It shows a list of users, by clicking on a specific list item, you can see selected user's posts.
    You can check each post and it's details including comments.
    
## UI
    
    Please don't cry when you see the design :D!.
    
    
## Next improvements

	1. Adding DataBinding to bind directly into views (xml).
	2. Moving from activities to only switching between fragments. For ex. replacing post fragment with comment fragment instead of starting an activity.
	3. Improve UI.
	
 

## Current issues
                
    1. Comments list is always 1. Need to fix it. Tried to debug, but didn't have enough time.